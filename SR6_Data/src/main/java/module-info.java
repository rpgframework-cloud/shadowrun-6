open module de.rpgframework.shadowrun6.data {
	exports de.rpgframework.shadowrun6.data;

	requires de.rpgframework.core;
	requires de.rpgframework.rules;
	requires transitive de.rpgframework.shadowrun6.core;
	requires shadowrun.common;
	requires simple.persist;
}