package de.rpgframework.shadowrun6.chargen.jfx.page;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.Page;
import org.prelle.javafx.layout.FlexGridPane;

import de.rpgframework.ResourceI18N;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.jfx.section.AppearanceSection;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterControllerProvider;
import de.rpgframework.shadowrun.chargen.jfx.section.QualitySection;
import de.rpgframework.shadowrun6.Shadowrun6Tools;
import de.rpgframework.shadowrun6.chargen.charctrl.SR6CharacterController;
import de.rpgframework.shadowrun6.chargen.jfx.SR6CharacterViewLayout;
import de.rpgframework.shadowrun6.chargen.jfx.section.AttributeSection;
import de.rpgframework.shadowrun6.chargen.jfx.section.BasicDataSection;
import de.rpgframework.shadowrun6.chargen.jfx.section.CritterPowerSection;
import de.rpgframework.shadowrun6.chargen.jfx.section.QualityPathsSection;
import de.rpgframework.shadowrun6.chargen.jfx.section.SR6QualitySection;
import javafx.scene.control.Label;

/**
 * @author prelle
 *
 */
public class BasicDataPage2 extends Page implements IShadowrunCharacterControllerProvider<SR6CharacterController> {

	private final static Logger logger = System.getLogger(BasicDataPage2.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(SR6CharacterViewLayout.class.getName());

	private SR6CharacterController control;

	private BasicDataSection secBaseData;
	private AppearanceSection secPortrait;
	private FlexGridPane flex;
	private AttributeSection secAttrib;
	private SR6QualitySection secQualities;
	private QualityPathsSection secQualPaths;
	private CritterPowerSection secCritterPower;

	private OptionalNodePane layout;

	//-------------------------------------------------------------------
	public BasicDataPage2() {
		super(ResourceI18N.get(RES, "page.basicdata.title"));
		logger.log(Level.DEBUG, "init<>");
		// Flow 1
		initBaseData();
		initPortrait();

		flex = new FlexGridPane();
		flex.setSpacing(20);
		flex.getChildren().addAll(secBaseData, secPortrait);

		// Flow 2
		initAttributes();
		initQualities();
		flex.getChildren().addAll(secAttrib, secQualities);

		initQualityPaths();
		initCritterPower();
		flex.getChildren().addAll(secQualPaths, secCritterPower);

		layout = new OptionalNodePane(flex, new Label("Select something to get a description"));
		setContent(layout);
//		setTitle("Basics");

	}

	//-------------------------------------------------------------------
	private void initBaseData() {
		secBaseData = new BasicDataSection(ResourceI18N.get(RES, "page.basicdata.section.basic.title"));
		secBaseData.setMaxHeight(Double.MAX_VALUE);
		FlexGridPane.setMinWidth(secBaseData, 4);
		FlexGridPane.setMinHeight(secBaseData, 5);
		FlexGridPane.setMediumWidth(secBaseData, 5);
		FlexGridPane.setMediumHeight(secBaseData, 4);
	}

	//-------------------------------------------------------------------
	private void initPortrait() {
		secPortrait = new AppearanceSection();
//		Image img = new Image("/mugshot.jpg");
//		secPortrait.iView.setImage(img);
		FlexGridPane.setMinWidth(secPortrait, 4);
		FlexGridPane.setMediumWidth(secPortrait, 8);
		FlexGridPane.setMinHeight(secPortrait, 7);
		FlexGridPane.setMediumHeight(secPortrait, 4);
	}

	//-------------------------------------------------------------------
	private void initAttributes() {
		secAttrib = new AttributeSection(ResourceI18N.get(RES, "page.basicdata.section.attributes.title"));
//		((AttributeSection)secAttrib).updateController(ctrl);

		FlexGridPane.setMinWidth(secAttrib, 4);
		FlexGridPane.setMediumWidth(secAttrib, 6);
		FlexGridPane.setMinHeight(secAttrib, 8);
		secAttrib.flexWidthProperty().addListener( (ov,o,n) -> {
			FlexGridPane.setMediumWidth(secAttrib, (Integer)n);
			flex.refresh();
		});
	}

	//-------------------------------------------------------------------
	private void initQualities() {
		secQualities = new SR6QualitySection();
		FlexGridPane.setMinWidth(secQualities, 4);
		FlexGridPane.setMinHeight(secQualities, 6);
		FlexGridPane.setMediumWidth(secQualities, 6);
		FlexGridPane.setMediumHeight(secQualities, 7);
		secQualities.showHelpForProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				layout.setOptional( new GenericDescriptionVBox( Shadowrun6Tools.requirementResolver(Locale.getDefault()),
						Shadowrun6Tools.modificationResolver(Locale.getDefault()), n.getModifyable()));
				layout.setTitle(n.getModifyable().getName());
			}
		});
	}

	//-------------------------------------------------------------------
	private void initQualityPaths() {
		secQualPaths= new QualityPathsSection();
//		((QualitySection)secQualities).updateController(ctrl);
		FlexGridPane.setMinWidth(secQualPaths, 4);
		FlexGridPane.setMediumWidth(secQualPaths, 4);
		FlexGridPane.setMinHeight(secQualPaths, 5);
		secQualPaths.showHelpForProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				layout.setOptional( new GenericDescriptionVBox( Shadowrun6Tools.requirementResolver(Locale.getDefault()),
						Shadowrun6Tools.modificationResolver(Locale.getDefault()), n.getModifyable()));
				layout.setTitle(n.getModifyable().getName());
			}
		});
	}

	//-------------------------------------------------------------------
	private void initCritterPower() {
		secCritterPower = new CritterPowerSection();
//		((QualitySection)secQualities).updateController(ctrl);
		FlexGridPane.setMinWidth(secCritterPower, 4);
		FlexGridPane.setMediumWidth(secCritterPower, 4);
		FlexGridPane.setMinHeight(secCritterPower, 5);
		secCritterPower.showHelpForProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				layout.setOptional( new GenericDescriptionVBox( Shadowrun6Tools.requirementResolver(Locale.getDefault()),
						Shadowrun6Tools.modificationResolver(Locale.getDefault()), n.getModifyable()));
				layout.setTitle(n.getModifyable().getName());
			}
		});
	}

	//-------------------------------------------------------------------
	public void setController(SR6CharacterController ctrl) {
		logger.log(Level.INFO, "setController");
		if (ctrl==null)
			throw new NullPointerException("controller is null");
		this.control = ctrl;
		((BasicDataSection)secBaseData).updateController(ctrl);
		((AttributeSection)secAttrib).updateController(ctrl);
		((QualitySection)secQualities).updateController(ctrl);
		((AppearanceSection)secPortrait).updateController(ctrl);
		((QualityPathsSection)secQualPaths).updateController(ctrl);
		((CritterPowerSection)secCritterPower).updateController(ctrl);
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterControllerProvider#getCharacterController()
	 */
	public SR6CharacterController getCharacterController() {
		return control;
	}

	//-------------------------------------------------------------------
	public void refresh() {
		secBaseData.refresh();
		secAttrib.refresh();
		secQualities.refresh();
		secQualPaths.refresh();
		secPortrait.refresh();
		secCritterPower.refresh();
	}

}
