package de.rpgframework.shadowrun6.chargen.jfx.page;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.Mode;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.Page;
import org.prelle.javafx.layout.FlexGridPane;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.ValueType;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.shadowrun.Focus;
import de.rpgframework.shadowrun.FocusValue;
import de.rpgframework.shadowrun.MetamagicOrEcho;
import de.rpgframework.shadowrun.MetamagicOrEchoValue;
import de.rpgframework.shadowrun.RitualValue;
import de.rpgframework.shadowrun.ShadowrunAttribute;
import de.rpgframework.shadowrun.Tradition;
import de.rpgframework.shadowrun.chargen.charctrl.IFocusController;
import de.rpgframework.shadowrun.chargen.jfx.pane.SpellDescriptionPane;
import de.rpgframework.shadowrun.chargen.jfx.section.FocusSection;
import de.rpgframework.shadowrun.chargen.jfx.section.MetamagicOrEchoSection;
import de.rpgframework.shadowrun.chargen.jfx.section.RitualSection;
import de.rpgframework.shadowrun.chargen.jfx.section.SpellSection;
import de.rpgframework.shadowrun6.SR6Spell;
import de.rpgframework.shadowrun6.Shadowrun6Core;
import de.rpgframework.shadowrun6.Shadowrun6Tools;
import de.rpgframework.shadowrun6.WorldType;
import de.rpgframework.shadowrun6.chargen.charctrl.SR6CharacterController;
import de.rpgframework.shadowrun6.chargen.jfx.SR6CharacterViewLayout;
import de.rpgframework.shadowrun6.chargen.jfx.section.AdeptPowerSection;
import de.rpgframework.shadowrun6.chargen.jfx.section.CombatSection;
import de.rpgframework.shadowrun6.chargen.jfx.selector.ChoiceSelectorDialog;
import javafx.scene.control.Label;

/**
 * @author prelle
 *
 */
public class MagicPage extends Page {

	private final static Logger logger = System.getLogger(MagicPage.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(SR6CharacterViewLayout.class.getName());

	private SR6CharacterController control;

	private AdeptPowerSection secAdeptPowers;
	private SpellSection<SR6Spell> secSpells;
	private MetamagicOrEchoSection secMeta;
	private RitualSection secRituals;
	private FocusSection secFoci;
	private CombatSection secCombat;

	private FlexGridPane flex;
	private OptionalNodePane layout;

	//-------------------------------------------------------------------
	public MagicPage() {
		super(ResourceI18N.get(RES, "page.magic.title"));
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		initPowers();
		initSpells();
		initMetamagic();
		initRituals();
		initFoci();
		initCombat();
	}

	//-------------------------------------------------------------------
	private void initPowers() {
		secAdeptPowers = new AdeptPowerSection(ResourceI18N.get(RES, "page.magic.section.adeptpowers"));
		secAdeptPowers.setMaxHeight(Double.MAX_VALUE);
		FlexGridPane.setMinWidth(secAdeptPowers, 4);
		FlexGridPane.setMinHeight(secAdeptPowers, 6);
		FlexGridPane.setMediumWidth(secAdeptPowers, 5);
		FlexGridPane.setMediumHeight(secAdeptPowers, 6);
		FlexGridPane.setMaxWidth(secAdeptPowers, 6);
		FlexGridPane.setMaxHeight(secAdeptPowers, 8);
	}

	//-------------------------------------------------------------------
	private void initSpells() {
		secSpells = new SpellSection<SR6Spell>(
				ResourceI18N.get(RES, "page.magic.section.spells"),
				Shadowrun6Tools.requirementResolver(Locale.getDefault()),
				Shadowrun6Tools.modificationResolver(Locale.getDefault())
				) {
			public void refresh() {
				super.refresh();

				cbTradition.getItems().setAll(Shadowrun6Core.getItemList(Tradition.class));

				if (model!=null)
					cbTradition.setValue(model.getTradition());
			}

		};
		secSpells.setMaxHeight(Double.MAX_VALUE);
		FlexGridPane.setMinWidth(secSpells, 4);
		FlexGridPane.setMinHeight(secSpells, 6);
		FlexGridPane.setMediumWidth(secSpells, 5);
		FlexGridPane.setMediumHeight(secSpells, 8);
	}

	//-------------------------------------------------------------------
	private void initMetamagic() {
		secMeta = new MetamagicOrEchoSection(
				ResourceI18N.get(RES, "page.magic.section.metamagic"),
				Shadowrun6Tools.requirementResolver(Locale.getDefault()),
				Shadowrun6Tools.modificationResolver(Locale.getDefault()),
				MetamagicOrEcho.Type.METAMAGIC
				);
		secMeta.setMaxHeight(Double.MAX_VALUE);
		FlexGridPane.setMinWidth(secMeta, 4);
		FlexGridPane.setMinHeight(secMeta, 6);
		FlexGridPane.setMediumWidth(secMeta, 5);
		FlexGridPane.setMediumHeight(secMeta, 8);
	}

	//-------------------------------------------------------------------
	private void initRituals() {
		secRituals = new RitualSection(
				ResourceI18N.get(RES, "page.magic.section.rituals"),
				Shadowrun6Tools.requirementResolver(Locale.getDefault()),
				Shadowrun6Tools.modificationResolver(Locale.getDefault())
				);
		secRituals.setMaxHeight(Double.MAX_VALUE);
		FlexGridPane.setMinWidth(secRituals, 4);
		FlexGridPane.setMinHeight(secRituals, 6);
		FlexGridPane.setMediumWidth(secRituals, 5);
		FlexGridPane.setMediumHeight(secRituals, 6);
	}

	//-------------------------------------------------------------------
	private void initCombat() {
		secCombat = new CombatSection(WorldType.ASTRAL);
		FlexGridPane.setMinWidth(secCombat, 6);
		FlexGridPane.setMinHeight(secCombat, 6);
		FlexGridPane.setMediumWidth(secCombat, 8);
	}

	//-------------------------------------------------------------------
	private void initFoci() {
		secFoci = new FocusSection(Shadowrun6Tools.requirementResolver(Locale.getDefault()), Shadowrun6Tools.modificationResolver(Locale.getDefault())) {
			@Override
			protected Decision[] requestUserDecisions(Focus value) {
				logger.log(Level.WARNING, "Present choice dialog");
				ChoiceSelectorDialog<Focus, FocusValue> dialog = new ChoiceSelectorDialog<>(control.getFocusController());
				Decision[] dec = dialog.apply(value, value.getChoices());
				logger.log(Level.WARNING, "decisions: "+Arrays.toString(dec));
				return dec;
			}
			public void refresh() {
				super.refresh();

				IFocusController ctrl = control.getFocusController();
				if (model!=null && ctrl!=null) {
					lbNum.setText(model.getFoci().size()+" / "+model.getAttribute(ShadowrunAttribute.MAGIC).getDistributed());
					lbSum.setText(ctrl.getFocusPointsLeft()+" / "+(model.getAttribute(ShadowrunAttribute.MAGIC).getDistributed()*5));
				}
			}
		};
		secFoci.setMaxHeight(Double.MAX_VALUE);
		FlexGridPane.setMinWidth(secFoci, 4);
		FlexGridPane.setMinHeight(secFoci, 6);
		FlexGridPane.setMediumWidth(secFoci, 5);
		FlexGridPane.setMediumHeight(secFoci, 6);
	}

	//-------------------------------------------------------------------
	private void initLayout() {

		flex = new FlexGridPane();
		flex.setSpacing(20);
		flex.getChildren().addAll(secCombat,secAdeptPowers, secSpells, secMeta, secRituals, secFoci);

		layout = new OptionalNodePane(flex, new Label("Select something to get a description"));
		setContent(layout);
		super.setMode(Mode.REGULAR);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		secSpells.showHelpForProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				logger.log(Level.INFO, "Show spell "+n);
				SpellDescriptionPane pane = new SpellDescriptionPane();
				pane.setData(n.getModifyable());
				layout.setOptional(pane);
				layout.setTitle(n.getModifyable().getName());
			}
		});
		secAdeptPowers.showHelpForProperty().addListener( (ov,o,n) -> showDescription(n));
		secMeta.showHelpForProperty().addListener( (ov,o,n) -> showDescription((MetamagicOrEchoValue)n));
		secRituals.showHelpForProperty().addListener( (ov,o,n) -> showDescription((RitualValue)n));
		secFoci.showHelpForProperty().addListener( (ov,o,n) -> showDescription((FocusValue)n));
	}

	//-------------------------------------------------------------------
	private void showDescription(ComplexDataItemValue<? extends ComplexDataItem> n) {
		logger.log(Level.INFO, "Show description "+n);
		if (n==null) {
			layout.setOptional(null);
		} else {
			layout.setOptional( new GenericDescriptionVBox(
					Shadowrun6Tools.requirementResolver(Locale.getDefault()),
					Shadowrun6Tools.modificationResolver(Locale.getDefault()), n.getModifyable()));
			layout.setTitle(n.getModifyable().getName());
		}
	}

	//-------------------------------------------------------------------
	public void setController(SR6CharacterController ctrl) {
		logger.log(Level.INFO, "setController");
		if (ctrl==null)
			throw new NullPointerException("controller is null");
		this.control = ctrl;

		secAdeptPowers.updateController(ctrl);
		secSpells.updateController(ctrl);
		secMeta.updateController(ctrl);
		secRituals.updateController(ctrl);
		secFoci.updateController(ctrl);
		secMeta.setOptionCallback(new ChoiceSelectorDialog<>(control.getMetamagicOrEchoController()));
		refresh();
	}

	//-------------------------------------------------------------------
	public void refresh() {
		secAdeptPowers.refresh();
		secSpells.refresh();
		secMeta.refresh();
		secRituals.refresh();
		secFoci.refresh();
		secCombat.setData(control.getModel());
	}

}
