package de.rpgframework.shadowrun6.export.json.model;

import java.util.List;

public class JSONSpell {
    public String name;
    public String id;
    public String category;
    public String type;
    public String duration;
    public String range;
    public int drain;
    public List<String> features;
    public boolean isAlchemistic;
    public List<String> influencedBy;
    public String page;
    public String description;
}
