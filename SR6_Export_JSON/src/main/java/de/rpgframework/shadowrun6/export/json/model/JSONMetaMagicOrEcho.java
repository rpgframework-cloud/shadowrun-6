package de.rpgframework.shadowrun6.export.json.model;

public class JSONMetaMagicOrEcho {
    public String name;
    public String page;
    public String description;
}
