package de.rpgframework.shadowrun6.export.json.model;

public class JSONItemAccessory {
    public String name;
    public String type;
    public String subType;
    public int rating;
    public String description;
}
