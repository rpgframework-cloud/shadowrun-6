package de.rpgframework.shadowrun6.export.json.model;

public class JSONSignatureManeuver {
    public String name;
    public String skill;
    public String action1Id;
    public String action1Name;
    public String action2Id;
    public String action2Name;
    public int cost;
}
