package de.rpgframework.shadowrun6.export.json.model;

import java.util.List;

public class JSONComplexForm {
    public String name;
    public String duration;
    public int fading;
    public List<String> influences;
    public String page;
    public String description;
}
