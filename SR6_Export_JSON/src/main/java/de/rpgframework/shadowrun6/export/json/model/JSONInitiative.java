package de.rpgframework.shadowrun6.export.json.model;

public class JSONInitiative {
    public String name;
    public String id;
    public int value;
    public String dice;
}
