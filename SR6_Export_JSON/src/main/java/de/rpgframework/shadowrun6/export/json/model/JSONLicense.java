package de.rpgframework.shadowrun6.export.json.model;

public class JSONLicense {
    public String name;
    public String sin;
    public String type;
    public String rating;
}
