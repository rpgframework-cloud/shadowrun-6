package de.rpgframework.shadowrun6.export.json.model;

public class JSONContact {
    public String name = "Unnamed";
    public String type;
    public int loyalty;
    public int influence;
    public String description;
    public int favors;
}
