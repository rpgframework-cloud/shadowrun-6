package de.rpgframework.shadowrun6.export.json.model;

import java.util.List;

public class JSONRitual {
    public String name;
    public int threshold;
    public List<String> features;
    public String page;
    public String description;
}
