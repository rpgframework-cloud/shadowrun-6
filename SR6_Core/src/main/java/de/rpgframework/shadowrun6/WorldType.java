package de.rpgframework.shadowrun6;

public enum WorldType {
	PHYSICAL,
	ASTRAL,
	MATRIX,
	MATRIX_UV
}