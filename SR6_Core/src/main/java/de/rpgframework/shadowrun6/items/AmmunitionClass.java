package de.rpgframework.shadowrun6.items;

/**
 * @author prelle
 *
 */
public enum AmmunitionClass {

	/** Holdout/Light Pistol/Machine Pistol */
	LIGHT,
	/** Heavy Pistol/SMG */
	HEAVY,
	RIFLES,
	TASER,
	INJECTION_DART,
	ASSAULT_CANNON,
	MACHINE_GUN,
	DMSO,
	SHOTGUN,
	BATTERY
	
}
