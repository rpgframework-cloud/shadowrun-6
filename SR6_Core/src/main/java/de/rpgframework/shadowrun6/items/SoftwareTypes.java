package de.rpgframework.shadowrun6.items;

/**
 * @author prelle
 *
 */
public enum SoftwareTypes {

	// Available on Drones and RCCs (and on Cyberdecks with autosoft_host)
	AUTOSOFT,
	// Available on Commlink, Cyberdeck, MTOC and RCC
	BASIC,
	// Available on Commlinks or Cyberdecks
	COMMLINK_APP,
	// Available on Cyberdecks
	HACKING,	
	// Special Autosoft available on RCCs
	E_SOFT,
	// Available on RCCs
	RCC,
	// Available on skill jacks (and on Cyberdecks with autosoft_host)
	SKILLSOFT,
	// Available only on MTOC
	TAC_APP
	
}
