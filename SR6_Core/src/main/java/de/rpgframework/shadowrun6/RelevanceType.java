package de.rpgframework.shadowrun6;

/**
 * @author stefa
 *
 */
public enum RelevanceType {
	COMBAT,
	SOCIAL,
	MAGICAL,
	MATRIX,
	MUNDANE,

}
