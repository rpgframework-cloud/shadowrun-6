package de.rpgframework.shadowrun6;

import de.rpgframework.genericrpg.data.ComplexDataItemValue;

/**
 * @author prelle
 *
 */
public class QualityPathStepValue extends ComplexDataItemValue<QualityPathStep> {

	//-------------------------------------------------------------------
	public QualityPathStepValue() {
	}

	//-------------------------------------------------------------------
	public QualityPathStepValue(QualityPathStep data) {
		super(data);
	}

}
