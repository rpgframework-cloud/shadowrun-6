package de.rpgframework.shadowrun6;

/**
 * @author prelle
 *
 */
public enum Selector {
	
	ATTRIBUTE_ANY,
	ATTRIBUTE_PHYSICAL,
	ATTRIBUTE_SOCIAL,
	
	ELEMENT,
	
	SKILL,

}
