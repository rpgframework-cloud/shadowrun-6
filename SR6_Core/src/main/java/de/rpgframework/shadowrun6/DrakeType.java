package de.rpgframework.shadowrun6;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id = "draketype")
public class DrakeType extends ComplexDataItem {

	//-------------------------------------------------------------------
	public DrakeType() {
	}

}
