package de.rpgframework.shadowrun6;

/**
 * @author prelle
 *
 */
public enum CreatePoints {

	ADJUST,
	ADJUST_DRAKE,
	ATTRIBUTES,
	CHARACTER_POINTS,
	CONTACT_POINTS,
	LIFEPATH_MODULES,
	NUYEN,
	MAXED_OUT_ATTRIBUTES,
	MAXED_OUT_SKILLS,
	SKILLS,

}
