package de.rpgframework.shadowrun6;

/**
 * @author prelle
 *
 */
public enum PriceModifiers {

	EVERYTHING,
	CLOTHING,
	/** Clothing and Armor */
	ARMOR,
	LIFESTYLE_NECESSITIES


}
