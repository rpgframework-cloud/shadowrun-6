package de.rpgframework.shadowrun6.persist;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import org.prelle.simplepersist.StringValueConverter;

import de.rpgframework.shadowrun.MagicOrResonanceType;
import de.rpgframework.shadowrun6.Shadowrun6Core;

public class MagicOrResonanceTypeConverter implements StringValueConverter<MagicOrResonanceType> {
	
	private final static Logger logger = System.getLogger("shadowrun.persist");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public MagicOrResonanceType read(String v) throws Exception {
		MagicOrResonanceType data = Shadowrun6Core.getItem(MagicOrResonanceType.class ,v);
		if (data==null) {
			logger.log(Level.ERROR, "No such magic or resonance type: "+v);
			throw new IllegalArgumentException("No such magic or resonance type: "+v);
		}
		return data;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(MagicOrResonanceType v) throws Exception {
		return v.getId();
	}
	
}