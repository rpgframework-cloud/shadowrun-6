package de.rpgframework.shadowrun6.modifications;

import de.rpgframework.genericrpg.data.CostType;

/**
 * @author prelle
 *
 */
public enum ShadowrunCostType implements CostType {

	KARMA,
	NUYEN,
	EDGE

}
