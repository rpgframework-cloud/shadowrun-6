package de.rpgframework.shadowrun6;

import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id = "sense")
public class Sense extends DataItem {

	//-------------------------------------------------------------------
	public Sense() {
	}

}
