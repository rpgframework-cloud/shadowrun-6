package de.rpgframework.shadowrun6;

import de.rpgframework.genericrpg.data.ComplexDataItemValue;

/**
 * @author prelle
 *
 */
public class LifepathModuleValue extends ComplexDataItemValue<LifepathModule> {

	//-------------------------------------------------------------------
	public LifepathModuleValue() {
	}

	//-------------------------------------------------------------------
	public LifepathModuleValue(LifepathModule data) {
		super(data);
	}

}
