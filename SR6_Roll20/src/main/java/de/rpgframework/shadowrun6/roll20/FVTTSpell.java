package de.rpgframework.shadowrun6.roll20;

public class FVTTSpell extends GenericFVTT {

	public String category;
	public int drain;
	public String duration;
	public String range;
	public String type;
	public String damage;
	public boolean alchemic;
	public boolean wild;
	public boolean isOpposed;
	public boolean withEssence;
	public boolean multiSense;

}
