package de.rpgframework.shadowrun6.chargen.gen.free;

import de.rpgframework.shadowrun6.chargen.gen.CommonSR6GeneratorSettings;

/**
 * @author prelle
 *
 */
public class SR6FreeSettings extends CommonSR6GeneratorSettings {

	//-------------------------------------------------------------------
	/**
	 */
	public SR6FreeSettings() {
	}

	//-------------------------------------------------------------------
	public String toAttributeString() {
		StringBuffer buf = new StringBuffer();
		return buf.toString();
	}

	//-------------------------------------------------------------------
	public String toSkillString() {
		StringBuffer buf = new StringBuffer();
		return buf.toString();
	}

}
