package de.rpgframework.shadowrun6.chargen.charctrl;

import de.rpgframework.shadowrun.chargen.charctrl.ILifestyleController;
import de.rpgframework.shadowrun6.SR6Lifestyle;

/**
 * @author prelle
 *
 */
public interface SR6LifestyleController extends ILifestyleController<SR6Lifestyle> {

}
