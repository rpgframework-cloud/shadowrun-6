package de.rpgframework.shadowrun6.chargen.charctrl;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.data.SkillSpecialization;
import de.rpgframework.shadowrun.chargen.charctrl.ISkillGenerator;
import de.rpgframework.shadowrun6.SR6Skill;
import de.rpgframework.shadowrun6.SR6SkillValue;

public interface SR6SkillGenerator extends SR6SkillController, ISkillGenerator<SR6Skill, SR6SkillValue> {

//	//-------------------------------------------------------------------
//	/**
//	 * Cab be selected with Skill Points?
//	 */
//	public Possible canSelectSpecializationWithPoints1(SR6SkillValue skillVal, SkillSpecialization<SR6Skill> spec);
//
//	//-------------------------------------------------------------------
//	/**
//	 * Cab be selected with knowledge skill points
//	 */
//	public Possible canSelectSpecializationWithPoints2(SR6SkillValue skillVal, SkillSpecialization<SR6Skill> spec);
//
//	//-------------------------------------------------------------------
//	/**
//	 * Can be selected with Karma?
//	 */
//	public Possible canSelectSpecializationWithPoints3(SR6SkillValue skillVal, SkillSpecialization<SR6Skill> spec);

}