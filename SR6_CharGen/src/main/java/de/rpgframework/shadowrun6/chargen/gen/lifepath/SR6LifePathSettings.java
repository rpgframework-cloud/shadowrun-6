package de.rpgframework.shadowrun6.chargen.gen.lifepath;

import java.util.ArrayList;
import java.util.List;

import de.rpgframework.shadowrun6.LifepathModuleValue;
import de.rpgframework.shadowrun6.SR6Quality;
import de.rpgframework.shadowrun6.SR6Skill;
import de.rpgframework.shadowrun6.Shadowrun6Core;
import de.rpgframework.shadowrun6.chargen.gen.CommonSR6GeneratorSettings;

/**
 * @author prelle
 *
 */
public class SR6LifePathSettings extends CommonSR6GeneratorSettings {

	private List<LifepathModuleValue> modules;

	private String nativeLanguage;
	private String bornQual1, bornQual2;
	private String childhoodArea;
	private String basicSkills;
	private String childQual1, childQual2;
	private String earlySkill;
	private String earlyQual1, earlyQual2;

	//-------------------------------------------------------------------
	/**
	 */
	public SR6LifePathSettings() {
		modules = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public List<LifepathModuleValue> getModules() {
		return modules;
	}

	//-------------------------------------------------------------------
	public void addModule(LifepathModuleValue module) {
		modules.add(module);
	}

	//-------------------------------------------------------------------
	public void removeModule(LifepathModuleValue module) {
		modules.remove(module);
	}

	//-------------------------------------------------------------------
	public String toAttributeString() {
		StringBuffer buf = new StringBuffer();
//		buf.append("\nCharacter Points remaining: "+characterPoints);
//		buf.append("\nCP converted to special attributes: "+cpBoughtSpecial);
//		buf.append("\nCP converted to regular attributes: "+cpBoughtAttrib);
//		for (Entry<ShadowrunAttribute,PerAttributePoints> ent : perAttrib.entrySet()) {
//			buf.append(String.format("\n%10s : %s", ent.getKey(), ent.getValue().toString()));
//		}
		return buf.toString();
	}

	//-------------------------------------------------------------------
	public String toSkillString() {
		StringBuffer buf = new StringBuffer();
//		buf.append("\nCharacter Points remaining: "+characterPoints);
//		buf.append("\nCP converted to skills: "+cpToSkills);
//		for (Entry<SR6SkillValue,PerSkillPoints> ent : perSkill.entrySet()) {
//			if (ent.getValue().getSum()>0)
//				buf.append(String.format("\n%10s : %s", ent.getKey().getSkill(), ent.getValue().toString()));
//		}
		return buf.toString();
	}

	//-------------------------------------------------------------------
	public SR6Quality getBornQuality1() {
		if (bornQual1 == null) return null;
		return Shadowrun6Core.getItem(SR6Quality.class, bornQual1);
	}

	//-------------------------------------------------------------------
	public void setBornQuality1(SR6Quality value) {
		if (value==null) this.bornQual1=null;
		else this.bornQual1 = value.getId();
	}

	//-------------------------------------------------------------------
	public SR6Quality getBornQuality2() {
		if (bornQual2 == null) return null;
		return Shadowrun6Core.getItem(SR6Quality.class, bornQual2);
	}

	//-------------------------------------------------------------------
	public void setBornQuality2(SR6Quality value) {
		if (value==null) this.bornQual2=null;
		else this.bornQual2 = value.getId();
	}

	//-------------------------------------------------------------------
	public String getNativeLanguage() {
		return nativeLanguage;
	}

	//-------------------------------------------------------------------
	public void setNativeLanguage(String value) {
		this.nativeLanguage = value;
	}

	//-------------------------------------------------------------------
	public SR6Quality getChildhoodQuality1() {
		if (childQual1 == null) return null;
		return Shadowrun6Core.getItem(SR6Quality.class, childQual1);
	}

	//-------------------------------------------------------------------
	public void setChildhoodQuality1(SR6Quality value) {
		if (value==null) this.childQual1=null;
		else this.childQual1 = value.getId();
	}

	//-------------------------------------------------------------------
	public SR6Quality getChildhoodQuality2() {
		if (childQual2 == null) return null;
		return Shadowrun6Core.getItem(SR6Quality.class, childQual2);
	}

	//-------------------------------------------------------------------
	public void setChildhoodQuality2(SR6Quality value) {
		if (value==null) this.childQual2=null;
		else this.childQual2 = value.getId();
	}

	//-------------------------------------------------------------------
	public String getChildhoodArea() {
		return childhoodArea;
	}

	//-------------------------------------------------------------------
	public void setChildhoodArea(String value) {
		this.childhoodArea = value;
	}

	//-------------------------------------------------------------------
	public List<SR6Skill> getChildhoodSkills() {
		List<SR6Skill> ret = new ArrayList<>();
		if (basicSkills!=null) {
			String[] keys = basicSkills.split(",");
			for (String key : keys) {
				SR6Skill skill = Shadowrun6Core.getItem(SR6Skill.class, key.trim());
				if (skill!=null) ret.add(skill);
			}
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public void setChildhoodSkills(List<SR6Skill> keys) {
		if (keys.size()>4)
			throw new IllegalArgumentException("Too many skills selected");
		basicSkills = String.join(",", keys.stream().map(key -> key.getId()).toList());
	}

	//-------------------------------------------------------------------
	public SR6Skill getEarlyAdultSkill() {
		SR6Skill ret = null;
		if (earlySkill!=null) {
			ret = Shadowrun6Core.getItem(SR6Skill.class, earlySkill.trim());
		}
		return ret;
	}
	
	//-------------------------------------------------------------------
	public void setEarlyAdultSkill(SR6Skill skill) {
		earlySkill=(skill!=null)?skill.getId():null;
	}
}
