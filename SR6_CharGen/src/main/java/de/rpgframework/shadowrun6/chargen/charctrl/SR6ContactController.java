package de.rpgframework.shadowrun6.chargen.charctrl;

import de.rpgframework.shadowrun.chargen.charctrl.IContactController;

/**
 * @author prelle
 *
 */
public interface SR6ContactController extends IContactController {

}
