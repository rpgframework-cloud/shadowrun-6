package de.rpgframework.shadowrun6.chargen.gen.priority;

import de.rpgframework.shadowrun6.chargen.charctrl.SR6AdeptPowerController;
import de.rpgframework.shadowrun6.chargen.charctrl.SR6CharacterController;

/**
 * @author prelle
 *
 */
public class SR6PriorityAdeptPowerGenerator extends SR6AdeptPowerController {

	//-------------------------------------------------------------------
	public SR6PriorityAdeptPowerGenerator(SR6CharacterController parent) {
		super(parent);
	}

}
