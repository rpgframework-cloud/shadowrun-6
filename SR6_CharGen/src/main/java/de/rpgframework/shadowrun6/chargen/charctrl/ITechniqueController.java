package de.rpgframework.shadowrun6.chargen.charctrl;

import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.shadowrun6.Technique;
import de.rpgframework.shadowrun6.TechniqueValue;

/**
 * @author prelle
 *
 */
public interface ITechniqueController extends ComplexDataItemController<Technique, TechniqueValue> {

}
