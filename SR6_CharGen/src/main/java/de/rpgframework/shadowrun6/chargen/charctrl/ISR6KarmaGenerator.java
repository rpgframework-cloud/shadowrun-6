package de.rpgframework.shadowrun6.chargen.charctrl;

import de.rpgframework.shadowrun6.chargen.gen.karma.SR6KarmaSettings;

/**
 * @author prelle
 *
 */
public interface ISR6KarmaGenerator extends SR6CharacterGenerator {

	//-------------------------------------------------------------------
	public SR6KarmaSettings getSettings();

}
